package com.coroutines.coroutinesApp

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

const val TAG2 = "SecondModel"

class SecondViewModel : ViewModel() {
    /**
    * LAUNCHING viewModelScope coroutine that finishes once the ViewModel is destroyed.
    */
    fun init() {
        viewModelScope.launch {
            while (true) {
                delay(1000L)
                Log.d(TAG2, "message from SecondViewModel")
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        Log.d(TAG2, "message from SecondViewModel finished")
    }
}
