package com.coroutines.coroutinesApp

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.coroutines.coroutinesApp.RetrofitInstance.api
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        /**
         *LAUNCHING A SIMPLE COROUTINE
         */
//        GlobalScope.launch {
//            delay(5000L)
//            Log.d("MainActivity", "Coroutine launched from thread ${Thread.currentThread().name}")
//        }
//        Log.d("MainActivity", "Hello from thread ${Thread.currentThread().name}")


        /**
         *LAUNCHING A COROUTINE WITH SUSPENDED FUNCTION
         */
//        GlobalScope.launch {
//            val doNetworkCall1 = doNetworkCall()
//            val doNetworkCall2 = doNetworkCall2()
//
//            Log.d(TAG, doNetworkCall1)
//            Log.d(TAG, doNetworkCall2)
//        }


        /**
         *LAUNCHING A COROUTINE WITH RUN BLOCKING (BLOCKING THE MAIN THREAD)
         */
//        Log.d(TAG, "Before run blocking")
//        runBlocking {
//
//            launch(Dispatchers.IO) {
//                delay(3000L)
//                Log.d(TAG, "Finished IO Coroutine 1")
//            }
//
//            launch(Dispatchers.IO) {
//                delay(3000L)
//                Log.d(TAG, "Finished IO Coroutine 2")
//            }
//
//            Log.d(TAG, "Start of runBlocking")
//            delay(5000L)
//            Log.d(TAG, "End of runBlocking")
//        }
////        Log.d(TAG, "After runBlocking")


        /**
         *LAUNCHING A CONTEXT COROUTINE (Starting in IO thread and getting the result in Main thread)
         */
//        GlobalScope.launch(Dispatchers.IO) {
//            Log.d(TAG, "Starting coroutine in thread ${Thread.currentThread().name}")
//            val answer = doNetworkCall()
//            withContext(Dispatchers.Main) {
//                Log.d(TAG, "Setting text in thread ${Thread.currentThread().name}")
//                tvDummy.text = answer
//            }
//        }


        /**
         * CANCELLING JOB AUTOMATICALLY
         */
//        val job = GlobalScope.launch(Dispatchers.Default) {
//            Log.d(TAG, "Starting long running calculation...")
//            withTimeout(3000L) {// CANCELING JOB WITH TIMEOUT AUTOMATICALLY
//                for (i in 30..40) {
//                    if (isActive) {
//                        Log.d(TAG, "Result for i = $i: ${fib(i)}")
//                    }
//                }
//            }
//
//            Log.d(TAG, "Ending long running calculation...")
//        }

        /**
         * CANCELING JOB MANUALLY
         */
//        runBlocking {
////            job.join() // SUSPENDS THE runBlocking until the job is complete
//            delay(2000L)
//            job.cancel()
////            Log.d(TAG, "Main thread is continuing...")
//            Log.d(TAG, "Canceled job!")
//        }

        /**
         *  LAUNCHING ASYNC COROUTINES AND AWAITING (await()) UNTIL THE PREVIOUS ONE COMPLETES
         */
//        GlobalScope.launch(Dispatchers.IO) {
//
//            val time = measureTimeMillis {
//                val answer1 = async { doNetworkCall() }
//                val answer2 = async { doNetworkCall2() }
//
//                Log.d(TAG, "Answer 1 is ${answer1.await()}")
//                Log.d(TAG, "Answer 1 is ${answer2.await()}")
//
//            }
//
//            Log.d(TAG, "Request took $time ms")
//        }

        /**
         * LAUNCHING lifecycleScope coroutine that finishes once the activity/fragment is destroyed.
         */

//        button.setOnClickListener {
//            lifecycleScope.launch {
//                while (true) {
//                    delay(1000L)
//
//                    Log.d(TAG, "Still running....")
//                }
//            }
//
//            GlobalScope.launch {
//                delay(5000L)
//                startActivity(Intent(this@MainActivity, SecondActivity::class.java))
//                finish()
//            }
//        }

//        lifecycleScope.launch(Dispatchers.IO) {
//            val response = api.getComments()
//            if (response.isSuccessful) {
//                response.body()?.let { comments ->
//                    for (comment in comments) {
//                        Log.d(TAG, comment.toString())
//                    }
//                }
//            }
//        }

        CoroutineScope(IO).launch {
            val response = api.getComments()
            if (response.isSuccessful) {
                response.body()?.let { comments ->
                    for (comment in comments) {
                        Log.d(TAG, comment.toString())
                    }
                }
            }
        }

    }

    private suspend fun doNetworkCall(): String {
        delay(3000L)
        return "This is the answer 1"
    }

    private suspend fun doNetworkCall2(): String {
        delay(3000L)
        return "This is the answer 2"
    }
//
//    private fun fib(n: Int): Long {
//        return when (n) {
//            0 -> 0
//            1 -> 1
//            else -> fib(n - 1) + fib(n - 2)
//        }
//    }

}
